import com.zuitt.example.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

//        System.out.println("Hello world!");

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
            contact1.setName("John Doe");
            contact1.setContactNumber("+639123456789");
            contact1.setAddress("Quezon City");

        Contact contact2 = new Contact();
            contact2.setName("Jane Doe");
            // contact2.setContactNumber("+639123456789");
            contact2.setAddress("Caloocan City");

        Contact contact3 = new Contact();
            contact3.setName("John Smith");
            // contact3.setContactNumber("+639123456789");
            // contact3.setAddress("Caloocan City");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);
        phonebook.setContacts(contact3);

        
        phonebook.printInfo();

    }
}