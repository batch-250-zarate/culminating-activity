package com.zuitt.example;

import java.util.ArrayList;

public class Contact {

    // Properties
    private String name;
    private String contactNumber;
    private String address;

    private Phonebook phonebook;

    // Constructor
        // default
        public Contact() {}

        // parameterized
        public Contact(String name, String contactNumber, String address) {
            this.name = name;
            this.contactNumber = contactNumber;
            this.address = address;
            this.phonebook = new Phonebook();
        }

    // Getter
    public String getName() {
            return this.name;
    }

    public String getContactNumber() {
            return this.contactNumber;
    }

    public String getAddress() {
            return this.address;
    }

    public ArrayList<Contact> getContact() {
            return  this.phonebook.getContacts();
    }

    // Setter
    public void setName(String name) {
            this.name = name;
    }
    public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
    }
    public void setAddress(String address) {
            this.address = address;
    }

    public void setPhonebook(Contact phonebook) {
            this.phonebook.setContacts(phonebook);
    }


}
