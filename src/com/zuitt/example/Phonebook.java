package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

        private ArrayList<Contact> contacts = new ArrayList<>();


        // default constructor
        public Phonebook() {}

        // parameterized constructor
        public Phonebook(ArrayList<Contact> contacts) {
            this.contacts = contacts;
        }

        // Getter
        public ArrayList<Contact> getContacts() {
            return this.contacts;
        }

        // Setter
        public void setContacts(Contact contacts) {
            this.contacts.add(contacts);
        }

        //method
        public void printInfo() {
            if (getContacts().isEmpty()) {
                System.out.println("--------------------");
                System.out.println("Phonebook is empty.");
                System.out.println("--------------------");
            } else {
                for (int i = 0; i < getContacts().size(); i++) {
                    System.out.println("Name: " + getContacts().get(i).getName());
                    System.out.println("--------------------");

                    if (getContacts().get(i).getContactNumber() == null) {
                        System.out.println("No registered number.");
                    } else {
                        System.out.println("Registered contact Number: " + getContacts().get(i).getContactNumber());
                    }
                    if (getContacts().get(i).getAddress() == null) {
                        System.out.println("No registered address.");
                    } else {
                        System.out.println("Registered address: " + getContacts().get(i).getAddress());
                        System.out.println("");
                    }
                }
            }
        }  
}


